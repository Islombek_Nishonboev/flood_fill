package com.android.smartstafftask.ui

import android.graphics.Bitmap
import android.widget.ImageView
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType


@StateStrategyType(AddToEndSingleStrategy::class)
interface MainView : MvpView {

    fun showProgress()

    fun hideProgress()

    fun showResult(bitmap: Bitmap, view: ImageView)

    fun showResults(bitmaps: Array<Bitmap>)


}