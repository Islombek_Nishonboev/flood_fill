package com.android.smartstafftask.ui

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.graphics.Bitmap
import android.graphics.Point
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import com.android.smartstafftask.R
import com.android.smartstafftask.databinding.ActivityMainBinding
import com.android.smartstafftask.databinding.DialogMainBinding
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxAdapterView
import com.jakewharton.rxbinding2.widget.RxSeekBar
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter

@SuppressLint("CheckResult")
class MainActivity : MvpAppCompatActivity(), MainView {

    @InjectPresenter
    lateinit var mPresenter: MainPresenter

    private lateinit var mBinding: ActivityMainBinding

    private val sizes = Point(64, 64)
    private var floodFillSpeed = 100
    private var firstFloodFillMethod = 0
    private var secondFloodFillMethod = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        initView()
    }

    private fun initView() {
        val firstBitmapFloodFiller = createFirstBitmapMotionEventObservable()
        val secondBitmapFloodFiller = createSecondBitmapMotionEventObservable()

        val bitmapGenerator = createBitmapGeneratorClickObservable()
        val bitmapSizePicker = createBitmapSizeClickObservable()

        bitmapGenerator
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext { showProgress() }
            .observeOn(Schedulers.computation())
            .map { mPresenter.generateRandomBitmaps(it.x, it.y) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                hideProgress()
                showResults(it)
            }

        firstBitmapFloodFiller
            .observeOn(AndroidSchedulers.mainThread())
            .filter { it.action == MotionEvent.ACTION_DOWN }
            .observeOn(Schedulers.computation())
            .map {
                mPresenter.executeFloodFilling(
                    firstFloodFillMethod,
                    mBinding.firstAlgorithmImage,
                    it
                )
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ showResult(it, mBinding.firstAlgorithmImage) }, { it.printStackTrace() })

        secondBitmapFloodFiller
            .observeOn(AndroidSchedulers.mainThread())
            .filter { it.action == MotionEvent.ACTION_DOWN }
            .observeOn(Schedulers.computation())
            .map {
                mPresenter.executeFloodFilling(
                    secondFloodFillMethod,
                    mBinding.secondAlgorithmImage,
                    it
                )
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ showResult(it, mBinding.secondAlgorithmImage) }, { it.printStackTrace() })

        bitmapSizePicker
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()

        createSpinnerItemsListeners()
        createSeekBarValueListener()
    }


    override fun showProgress() {
        mBinding.progressMain.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        mBinding.progressMain.visibility = View.GONE
    }

    override fun showResult(bitmap: Bitmap, view: ImageView) {

        val emptyBitmap = Bitmap.createBitmap(bitmap.width, bitmap.height, bitmap.config)
        if (bitmap.sameAs(emptyBitmap)) return

        view.setImageBitmap(bitmap)
    }

    override fun showResults(bitmaps: Array<Bitmap>) {
        with(mBinding) {
            firstAlgorithmImage.setImageBitmap(bitmaps[0])
            secondAlgorithmImage.setImageBitmap(bitmaps[1])
        }
    }

    // function to create first image motion
    @SuppressLint("ClickableViewAccessibility")
    private fun createFirstBitmapMotionEventObservable(): Observable<MotionEvent> =
        Observable.create { emitter ->
            mBinding.firstAlgorithmImage.setOnTouchListener { view, event ->
                if ((view as ImageView).drawable != null) emitter.onNext(event)
                true
            }

            emitter.setCancellable { mBinding.firstAlgorithmImage.setOnTouchListener(null) }
        }

    // function to create first image motion
    @SuppressLint("ClickableViewAccessibility")
    private fun createSecondBitmapMotionEventObservable(): Observable<MotionEvent> =
        Observable.create { emitter ->
            mBinding.secondAlgorithmImage.setOnTouchListener { view, event ->
                if ((view as ImageView).drawable != null) emitter.onNext(event)
                true
            }

            emitter.setCancellable { mBinding.secondAlgorithmImage.setOnTouchListener(null) }
        }

    // function to generate bitmap and set it to ImageViews
    private fun createBitmapGeneratorClickObservable(): Observable<Point> =
        Observable.create { emitter ->
            RxView.clicks(mBinding.btnGenerate).subscribe {
                mBinding.firstAlgorithmImage.setImageDrawable(null)
                mBinding.secondAlgorithmImage.setImageDrawable(null)

                emitter.onNext(sizes)
            }
        }

    //function to change size of bitmap
    private fun createBitmapSizeClickObservable(): Completable = Completable.create { emitter ->
        val dialogBinding = DialogMainBinding.inflate(layoutInflater)

        val dialogBuilder = AlertDialog.Builder(this)
        val dialog = dialogBuilder.create()
        dialog.setView(dialogBinding.root)

        val width = dialogBinding.root.findViewById<EditText>(R.id.edit_width)
        val height = dialogBinding.root.findViewById<EditText>(R.id.edit_height)

        dialogBinding.okButton.isEnabled = false

        val widthWatcherObservable = RxTextView.textChanges(width)
        val heightWatcherObservable = RxTextView.textChanges(width)

        Observable.merge(widthWatcherObservable, heightWatcherObservable).subscribe {
            dialogBinding.okButton.isEnabled =
                (width.text.isNotEmpty()) && (height.text.isNotEmpty())
        }

        RxView.clicks(mBinding.btnSize).subscribe { dialog.show() }
        mBinding.btnSize.setOnClickListener { dialog.show() }

        RxView.clicks(dialogBinding.okButton).subscribe {
            sizes.x = width.text.toString().toInt()
            sizes.y = height.text.toString().toInt()

            width.hint = width.text
            height.hint = height.text

            dialog.dismiss()
            emitter.onComplete()
        }

        RxView.clicks(dialogBinding.cancelButton).subscribe { dialog.dismiss() }
    }

    //function to select algorithms
    private fun createSpinnerItemsListeners() {
        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.algorithms_array, android.R.layout.simple_spinner_item
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        mBinding.firstSpinner.adapter = adapter
        mBinding.secondSpinner.adapter = adapter

        RxAdapterView.itemSelections(mBinding.firstSpinner)
            .subscribeOn(AndroidSchedulers.mainThread())
            .subscribe { firstFloodFillMethod = it }

        RxAdapterView.itemSelections(mBinding.secondSpinner)
            .subscribeOn(AndroidSchedulers.mainThread())
            .subscribe { firstFloodFillMethod = it }
    }

    // function to control speed of motion
    private fun createSeekBarValueListener() {
        RxSeekBar.userChanges(mBinding.seekbarSpeed).subscribe { floodFillSpeed = it }
    }
}